// Assignment 4
// Task 1
let text = 'The Quick Brown Fox'
console.log(`Task 1 Input>> ${text}`)
function charReverseHandler (text) {
    // your code here
    let newText = [];
    for(let i=0; i<text.length; i++){
        if(text[i]==text[i].toLowerCase()){
            newText.push(text[i].toUpperCase())
        }else {
            newText.push(text[i].toLowerCase())
        }
    }
    return newText.join("")
}
console.log(`Output>> ${charReverseHandler(text)}`)

// Task 2
function createListHandler () {
    // your code here
    todoForm = document.querySelector(".todo__form");
    todoList = document.querySelector(".todo__list");
    todoInput = document.querySelector(".todo__input");
    todoButton = document.querySelector(".todo__button");
    checked = document.querySelector('#checkbox');

const items=[
    'item1',
    'item2',
];

todoForm.addEventListener("submit", (event) => {
    event.preventDefault();
    let newItem = todoInput.value;
      if(newItem){
        items.push(newItem);
        createNewsList(items, todoList);
        todoInput.value = "";
      }
  });

function createNewsList(newsList, parent) {
    parent.innerHTML = "";
    newsList.forEach((itemNews, index) => {
      parent.innerHTML += `
      <li class="todo__item"> 
      <input class='checkbox' type="checkbox">
        ${index + 1} ${itemNews}
        <div class="delete"><img src="delete-button.png" alt=""></div>
      </li>
    `;
    });

    document.querySelectorAll(".delete").forEach((btn, i) => {
      btn.addEventListener("click", () => {
        btn.parentElement.remove();
        items.splice(i, 1);
        createNewsList(items, todoList);
      });
    });
  }
  createNewsList(items, todoList);
}
createListHandler();

// Task 3
function findDublicatedHandler (arr) {
    // your code here
      let dublicatedArr = []
      arr.filter((elem,index,arr) => {
          if (arr.indexOf(elem) != index) {
              dublicatedArr.push(elem)
          }
      })
      console.log(dublicatedArr)
  }
  findDublicatedHandler([1, 30, -12, 20, 30, -1, 20, -12, 34, 56])

// Task 4
function unionHandler (arr1,arr2) {
  // your code here
  let unionArr = []
  unionArr = [... new Set([...arr1, ...arr2])].sort(function(a, b){return a - b;})
  return unionArr;
}
console.log(unionHandler([1,2,3,4,5], [1,'a','f',3,4,5]))

// Task 5
function unionHandlers (array) {
    // your code here
    return array.filter(elem => elem);
}
console.log(unionHandlers([1,2,3, 'string' , false, null, undefined, '', 0 ]))

// Task 6
function reverseStringHandler (text) {
  // your code here
  let sortedText = '';  
   sortedText = text.split('').sort((a, b) => a.localeCompare(b)).join('');
   return sortedText;
}
console.log(reverseStringHandler('AAaDoniyoLLlrbek'));
